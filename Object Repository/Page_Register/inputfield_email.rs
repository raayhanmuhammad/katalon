<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>inputfield_email</name>
   <tag></tag>
   <elementGuidId>d9df79c4-75cf-4a02-8ae8-d0631074c55d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#exampleInputEmail1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='exampleInputEmail1']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a1ef494f-81eb-41ce-bb3d-1927c3fae573</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>a49da289-4acc-4641-a9c3-e10895c9253a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>efd91cba-e29f-4ce9-8922-ad40faab6318</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>exampleInputEmail1</value>
      <webElementGuid>247cde80-a43f-4bc7-af53-af192801ee90</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Contoh: johndee@gmail.com</value>
      <webElementGuid>c9c261a9-41f1-4a24-bb2f-4d4220473d33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;exampleInputEmail1&quot;)</value>
      <webElementGuid>310c8453-eb2a-4aba-8c08-5ab989bbf202</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='exampleInputEmail1']</value>
      <webElementGuid>c8beebe7-5248-4161-8889-0107af772563</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/form/div[2]/input</value>
      <webElementGuid>dce4249a-7e01-42c4-afee-1c76f132cae8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/input</value>
      <webElementGuid>8f921805-3750-4b6a-b53a-8b97ff159789</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'email' and @id = 'exampleInputEmail1' and @placeholder = 'Contoh: johndee@gmail.com']</value>
      <webElementGuid>4dad0443-c05c-40ac-82a9-64d932ac7aab</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
