<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Product_name</name>
   <tag></tag>
   <elementGuidId>891c6b35-3a07-46b2-b009-f3bd0935a735</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card.product > div.card-body > div > h5.card-title</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Masukkan Harga Tawarmu'])[1]/following::h5[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>ab596706-3abd-4e1f-94e5-e006ba0ebe57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-title</value>
      <webElementGuid>745944f4-cbc6-4948-aa01-9a3b6d52db08</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Gitar </value>
      <webElementGuid>b8f01724-60b5-4c6f-9382-bb65c289f93a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;fade modal show&quot;]/div[@class=&quot;modal-dialog modal-sm modal-dialog-centered&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;card product&quot;]/div[@class=&quot;card-body&quot;]/div[1]/h5[@class=&quot;card-title&quot;]</value>
      <webElementGuid>00eb4389-2428-405d-9fd4-c1447db03b80</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masukkan Harga Tawarmu'])[1]/following::h5[1]</value>
      <webElementGuid>bc09bc17-ef8c-40a0-8825-b8358cd12f41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hary Utama Kurniawan'])[1]/following::h5[1]</value>
      <webElementGuid>f369ee39-63c5-4c09-a81e-bffa8f13b024</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Harga Tawar'])[1]/preceding::h5[1]</value>
      <webElementGuid>46577398-a58c-4dae-b151-c9022e66711c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kirim'])[1]/preceding::h5[1]</value>
      <webElementGuid>d31bc6e1-0369-4d64-afa2-f7afd4199397</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/h5</value>
      <webElementGuid>347c06f5-692c-40c2-849f-74ea63890294</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'Gitar ' or . = 'Gitar ')]</value>
      <webElementGuid>bc72df23-2402-44fd-a614-164e376ec26c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
