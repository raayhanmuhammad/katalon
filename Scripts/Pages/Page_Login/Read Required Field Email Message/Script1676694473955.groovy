import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebDriver Driver = DriverFactory.getWebDriver()

JavascriptExecutor js = (JavascriptExecutor)Driver;

WebElement field_email = Driver.findElement(By.xpath("//input[@id='exampleInputEmail1']"));

String act_message = (String)js.executeScript("return arguments[0].validationMessage;", field_email);

KeywordUtil.logInfo('Error Message: ' + act_message)

println(exp_message)

WebUI.verifyMatch(act_message, exp_message, false)