import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.verifyElementVisible(findTestObject('Page_Home/btn_baju'))

WebUI.verifyElementVisible(findTestObject('Page_Home/btn_elektronik'))

WebUI.verifyElementVisible(findTestObject('Page_Home/btn_hoby'))

WebUI.verifyElementVisible(findTestObject('Page_Home/btn_home'))

WebUI.verifyElementVisible(findTestObject('Page_Home/btn_jual'))

WebUI.verifyElementVisible(findTestObject('Page_Home/btn_kendaraan'))

WebUI.verifyElementVisible(findTestObject('Page_Home/btn_kesehatan'))

WebUI.verifyElementVisible(findTestObject('Page_Home/btn_semua'))

WebUI.verifyElementVisible(findTestObject('Page_Home/container_promo'))

WebUI.verifyElementVisible(findTestObject('Page_Home/div_product_card'))

WebUI.verifyElementVisible(findTestObject('Page_Home/img_product_image'))

WebUI.verifyElementVisible(findTestObject('Page_Home/label_telusuri_kategori'))

WebUI.verifyElementVisible(findTestObject('Page_Home/product_category'))

WebUI.verifyElementVisible(findTestObject('Page_Home/product_name'))

WebUI.verifyElementVisible(findTestObject('Page_Home/product_price'))

