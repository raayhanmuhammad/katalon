import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject



import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//WebUI.click(findTestObject('Page_Product_Detail/btn_saya_tertarik_dan_ingin_nego'))

WebDriver driver = DriverFactory.getWebDriver()

JavascriptExecutor js = (JavascriptExecutor) driver

WebElement btn_tertarik = driver.findElement(By.xpath("//button[contains(text(),'Saya tertarik dan ingin nego')]"))

WebElement Container = driver.findElement(By.xpath("//body/div[@id='root']/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]"))

js.executeScript("arguments[0].scrollIntoView(true);", Container)

sleep(2000)

btn_tertarik.click()

