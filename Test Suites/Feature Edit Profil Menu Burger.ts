<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Feature Edit Profil Menu Burger</description>
   <name>Feature Edit Profil Menu Burger</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>8d18d1ef-1aaf-4522-bed9-1abd9d0d5722</testSuiteGuid>
   <testCaseLink>
      <guid>78ffab6b-1d54-4083-a8f0-0634e843eb38</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Edit_Profile_Menu_Burger/EPMB003 - User does not input name and number phone</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>55001e4e-225e-446e-a69e-efc9433f2e6f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Edit_Profile_Menu_Burger/EPMB002 - User does not input name</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>69c16d97-ee97-45cb-aa94-962b12bd1226</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Edit_Profile_Menu_Burger/EPMB001 - User edit profile with valid data</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>530d8d05-8474-4b77-8a40-93f94e028cc0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Edit_Profile_Menu_Burger/EPMB004 - User input number handphone with symbol</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>865dc657-1c6c-455d-9e3d-047f768c17e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Edit_Profile_Menu_Burger/EPMB005 - User does not input photo profile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
