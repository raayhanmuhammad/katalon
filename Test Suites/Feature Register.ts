<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Feature Register</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>ca278ea1-546b-42c6-8364-6f501c7b7efd</testSuiteGuid>
   <testCaseLink>
      <guid>e7830f2b-248c-4a45-9656-5f3a6086254b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Register/Feature Register/REG001 - User want to register without input nama, email and password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1d61efb2-99f7-4bfb-b9fa-249bfd0f225a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Register/Feature Register/REG002 - User want to register without input nama</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>bfcf41ff-150e-4958-b6f4-9f85e2d504bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Register/Feature Register/REG003 - User want to register without input email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>75ac0c06-15b6-4468-aaa6-72ff02cd6827</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Register/Feature Register/REG004 - User want to register without input password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>22ebe732-2015-45e4-b457-0c9c4ed8ed8d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Register/Feature Register/REG005 - User want to register without using format email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>99917cc6-ba3f-4027-84c0-b9d18a71dcff</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>00ad6331-be16-4618-ad39-028de2ec896f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Register/Feature Register/REG006 - User want to register with incomplete format email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>65fc5dfd-c58a-4af7-aa8e-669c0e9cf164</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>59942978-c216-43e1-8f07-cc80589afbc9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Register/Feature Register/REG007 - User want to register using correct data</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>76e3f800-a44a-4598-aeb0-e0182ba74a1f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>5dfb1277-c89e-4d29-a38d-c0cb3fb25f10</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Register/Feature Register/REG008 - User want to register using registered email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>cc280516-c94f-4909-9115-8fed8f9449ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Register/Feature Register/REG009 - User want to register using unverified email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4de8fe88-de0c-4073-b2e0-0cc09c063980</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Register/Feature Register/REG010 - User want to access login page</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
