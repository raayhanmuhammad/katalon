<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Feature Buy Product</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>90a459dd-e1ef-4c0d-b12c-2f5721224649</testSuiteGuid>
   <testCaseLink>
      <guid>7716c0ef-26c3-485d-862d-ae12b070eb49</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <iterationNameVariable>
         <defaultValue>'syammaryw2@gmail.com'</defaultValue>
         <description></description>
         <id>f8c4e180-13d5-45f4-a5e3-24ed4ea7fd3f</id>
         <masked>false</masked>
         <name>username</name>
      </iterationNameVariable>
      <testCaseId>Test Cases/Step Definition/Page_Home/Feature Buy Product/BPO001 - Buyer want to buy a product from home page</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>c9d7358d-1bbc-4ebf-b6ac-3b8c2bdeab7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Home/Feature Buy Product/BPO002 - Buyer want to buy a product from home page without login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
