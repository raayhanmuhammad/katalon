@EditProduct
Feature: Edit Product From BurgerMenu
	As a user, I want to add a product in Secondhand Web.
	
	@EDP001
	Scenario: EDP001 - User want to edit a product from Burger Menu
	
	Then User open browser
	Then User login to the website
	Then User click Burger Menu
	Then User click Produk
	Then User click Edit
	Then User input Nama Produk
	Then User input Harga Produk
	Then User choose the category
	Then User input the description
	Then User click Terbitkan button
	Then User close browser