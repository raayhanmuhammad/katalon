@Login
Feature: Login
As a user, i want to login in Secondhand Store web.

@LGI001
Scenario: LGI001 - User want to login without input email and password
Then User open browser
Then User click on Masuk button at Homepage
Then User click on Masuk button at Login page
Then Error message will be shown 'Please fill out this field.' at Email field
Then User close browser

@LGI002
Scenario: LGI002 - User want to login only input registered email
Then User open browser
Then User click on Masuk button at Homepage
Then User input registered email only 'raayhanmuhammad.kerjaan@gmail.com'
Then User click on Masuk button at Login page
Then Error message will be shown 'Please fill out this field.' at Password field
Then User close browser

@LGI003
Scenario: LGI003 - User want to login only input registered password
Then User open browser
Then User click on Masuk button at Homepage
Then User input registered password only 'rayhan691971'
Then User click on Masuk button at Login page
Then Error message will be shown 'Please fill out this field.' at Email field
Then User close browser

@LGI004
Scenario: LGI004 - User want to login without using format email
Then User open browser
Then User click on Masuk button at Homepage
Then User input email without format 'raayhanmuhammad.kerjaan'
Then User input registered password 'rayhan691971'
Then User click on Masuk button at Login page
Then Error message will be shown "Please include an '@' in the email address. 'raayhanmuhammad.kerjaan' is missing an '@'." at Email field
Then User close browser

@LGI005
Scenario: LGI005 - User want to login with incomplete format email
Then User open browser
Then User click on Masuk button at Homepage
Then User input email with incomplete format 'raayhanmuhammad.kerjaan@'
Then User input registered password 'rayhan691971'
Then User click on Masuk button at Login page
Then Error message will be shown "Please enter a part following '@'. 'raayhanmuhammad.kerjaan@' is incomplete." at Email field
Then User close browser

@LGI006
Scenario: LGI006 - User want to login using correct credential
Then User open browser
Then User click on Masuk button at Homepage
Then User input registered email 'raayhanmuhammad.kerjaan@gmail.com'
Then User input registered password 'rayhan691971'
Then User click on Masuk button at Login page
Then User close browser

@LGI007
Scenario: LGI007 - User want to login using valid email and invalid password
Then User open browser
Then User click on Masuk button at Homepage
Then User input valid email 'raayhanmuhammad.kerjaan@gmail.com'
Then User input invalid password 'rayhan66'
Then User click on Masuk button at Login page
Then Popup message will be shown 'Password anda salah!'
Then User close browser

@LGI008
Scenario: LGI008 - User want to login using invalid email and valid password
Then User open browser
Then User click on Masuk button at Homepage
Then User input invalid email 'raayhanmuhammmmad@gmail.com'
Then User input valid password 'rayhan691971'
Then User click on Masuk button at Login page
Then Popup message will be shown 'Akun tidak ditemukan'
Then User close browser

@LGI009
Scenario: LGI009 - User want to login using invalid email and invalid password
Then User open browser
Then User click on Masuk button at Homepage
Then User input invalid email 'raayhanmuhammmmad@gmail.com'
Then User input invalid password 'rayhan66'
Then User click on Masuk button at Login page
Then Popup message will be shown 'Akun tidak ditemukan'
Then User close browser

@LGI010
Scenario: LGI010 - User want to login using unverified account
Then User open browser
Then User click on Masuk button at Homepage
Then User input unverified email 'rayhanmuhammad@gmail.com'
Then User input unverified password 'rayhan691971'
Then User click on Masuk button at Login page
Then Popup message will be shown 'Silahkan cek email anda untuk melakukan verifikasi terlebih dahulu'
Then User close browser

@LGI011
Scenario: LGI011 - User want to access register page
Then User open browser
Then User click on Masuk button at Homepage
Then User click on link Daftar disini
Then User close browser


