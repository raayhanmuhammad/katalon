@profile
Feature: profile
  As a user, i want to edit products in Secondhand Store web.

 @PR001
 Scenario: PR001 - User ingin mengedit profile setelah membuat akun
    Then User open browser
    Then User login web using correct credential 
    #Then User click user menu 
		Then User click dropdown option profile
		Then User input nama profile 'rayhan muhammad'
		Then User input dropdown kota 'bandung'
		Then User input alamat 'jalan jalan'
		Then User input no hp '081234567890'
		Then User click submit at profile page
		Then Success message 'Berhasil update profile'
		Then User close browser

@PR002
 Scenario: PR002 - User tidak mengisi kolom nama
    Then User open browser
    Then User login web using correct credential 
    #Then user click user menu 
		Then User click dropdown option profile
		Then User input dropdown kota 'bandung'
		Then User input alamat 'jalan jalan'
		Then User input no hp '081234567890'
		Then User click submit at profile page
		Then Success message 'User harus mengisi kolom nama'
		Then User close browser
 
 @PR003
 Scenario: PR003 - User tidak mengisi kolom nama dan no hp
    Then User open browser
    Then User login web using correct credential
    #Then user click user menu 
		Then User click dropdown option profile
		Then User input dropdown kota 'bandung'
		Then User input alamat 'jalan jalan'
		Then User click submit at profile page
		Then Success message 'User harus mengisi kolom nama dan no hp'
		Then User close browser 
		
	@PR004
 Scenario: PR004 - User mengisi no hp dengan symbol
    Then User open browser 
    Then User login web using correct credential
    #Then user click user menu 
		Then User click dropdown option profile
		Then User input nama profile 'rayhan muhammad'
		Then User input dropdown kota 'bandung'
		Then User input alamat 'jalan jalan'
		Then User input no hp '!@#$%^!@#$%^&*'
		Then User click submit at profile page
		Then Success message 'User harus mengisi no hp dengan benar'
		Then User close browser
		
	@PR005
 Scenario: PR005 - User tidak menginput foto profile
    Then User open browser
    Then User login web using correct credential
    #Then user click user menu 
		Then User click dropdown option profile
		Then User input nama profile 'rayhan muhammad'
		Then User input dropdown kota 'bandung'
		Then User input alamat 'jalan jalan'
		Then User input no hp '081234567890'
		Then User click submit at profile page
		Then Success message 'User harus mengisi foto profile'
		Then User close browser 
    