@Register
Feature: Register
As a user, i want to register in Secondhand Store web.

@REG001
Scenario: REG001 - User want to register without input nama, email and password
Then User open browser
Then User click on Masuk button at Homepage
Then User click on link Daftar at Login page
Then User click on Daftar button at Register page
Then Error message will be shown 'Please fill out this field.' at Nama field
Then User close browser

@REG002
Scenario: REG002 - User want to register without input nama
Then User open browser
Then User click on Masuk button at Homepage
Then User click on link Daftar at Login page
Then User input Email 'testadmin@gmail.com'
Then User input password 'xxxxxx'
Then User click on Daftar button at Register page
Then Error message will be shown 'Please fill out this field.' at Nama field
Then User close browser

@REG003
Scenario: REG003 - User want to register without input email
Then User open browser
Then User click on Masuk button at Homepage
Then User click on link Daftar at Login page
Then User input Nama 'rayhan muhammad'
Then User input password 'xxxxxx'
Then User click on Daftar button at Register page
Then Error message will be shown 'Please fill out this field.' at Email field
Then User close browser

@REG004
Scenario: REG004 - User want to register without input password
Then User open browser
Then User click on Masuk button at Homepage
Then User click on link Daftar at Login page
Then User input Nama 'rayhan muhammad'
Then User input email 'testadmin@gmail.com'
Then User click on Daftar button at Register page
Then Error message will be shown 'Please fill out this field.' at Password field
Then User close browser

@REG005
Scenario: REG005 - User want to register without using format email
Then User open browser
Then User click on Masuk button at Homepage
Then User click on link Daftar at Login page
Then User input Nama 'rayhan muhammad'
Then User input email without format 'testadmin'
Then User password 'xxxxxx'
Then User click on Daftar button at Register page
Then Error message will be shown "Please include an '@' in the email address. 'testadmin' is missing an '@'." at Email field
Then User close browser

@REG006
Scenario: REG006 - User want to register with incomplete format email
Then User open browser
Then User click on Masuk button at Homepage
Then User click on link Daftar at Login page
Then User input nama 'rayhan muhammad'
Then User input email with inclomplete format 'testadmin@'
Then User input password 'xxxxxx'
Then User click on Daftar button at Register page
Then Error message will be shown "Please enter a part following '@'. 'testadmin@' is incomplete." at Email field
Then User close browser

@REG007
Scenario: REG007 - User want to register using correct data
Then User open browser
Then User click on Masuk button at Homepage
Then User click on link Daftar at Login page
Then User input nama 'rayhan muhammad'
Then User input email 'testadmin123456@gmail.com'
Then User input password 'xxxxxxxx'
Then User click on Daftar button at Register page
Then popup meessage will be shown 'Silahkan verifikasi email agar dapat menggunakan layanan kami'
Then User close browser

@REG008
Scenario: REG008 - User want to register using registered email
Then User open browser
Then User click on Masuk button at Homepage
Then User click on link Daftar at Login page
Then User input nama 'rayhan muhammad'
Then User input email 'raayhanmuhammad.kerjaan@gmail.com'
Then User input password 'rayhan691971'
Then User click on Daftar button at Register page
Then popup message will be shown 'Email sudah digunakan'
Then User close browser

@REG009
Scenario: REG009 - User want to register using unverified email
Then User open browser
Then User click on Masuk button at Homepage
Then User click on link Daftar at Login page
Then User input nama 'rayhan muhammad'
Then User input email 'rayhanmuhammad@gmail.com'
Then User input password 'rayhan691971'
Then User click on Daftar button at Register page
Then popup message will be shown 'Email sudah digunakan'
Then User close browser

@REG010
Scenario: REG010 - User want to access login page
Then User open browser
Then User click on Masuk button at Homepage
Then User click on link Daftar at Login page
Then User click on link Masuk disini
Then User close browser

