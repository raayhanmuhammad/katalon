@AddProductFromBurgermenu
Feature: Add Product From Home
	As a user, I want to add a product in Secondhand Web.
	
	@ADP001
	Scenario: ADP001 - User want to add a product from Burger Menu
	Then User open browser
	Then User login to the website
	Then User click Burger Menu
	Then User click Tambah Produk
	Then User input Nama Produk
	Then User input Harga Produk
	Then User choose the category
	Then User input the description
	Then User click Terbitkan button
	Then User close browser
	
	@ADP002
	Scenario: ADP002 - User want to preview a product from Burger Menu
	Then User open browser
	Then User login to the website
	Then User click Burger Menu
	Then User click Tambah produk
	Then User input Nama Produk
	Then User input Harga Produk
	Then User choose the category
	Then User input the description
	Then User click Terbitkan button
	Then User close browser