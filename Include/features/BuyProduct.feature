@BuyProduct
Feature: Buy Product
	As a user, i want to buy a product in Secondhand Store web.
	
	@BPO001
	Scenario: BPO001 - Buyer want to buy a product from home page
		Then Buyer click on Masuk button at Homepage
		Then Buyer input registered email 'kyoshitsugi@gmail.com'
		Then Buyer input registered password 'admin123'
		Then Buyer click on Masuk button at Login page
		Then Buyer choose the product
		Then Buyer click Saya tertarik dan ingin nego button
		Then Buyer input Prefered Price
		Then Buyer click Kirim Button
		Then Success Alert 'Harga tawarmu berhasil dikirim ke penjual' will be shown
		
	@BPO002
	Scenario: BPO002 - Buyer want to buy a product from home page without login
		Then Buyer choose the product
		Then Buyer click Saya tertarik dan ingin nego button
		Then Buyer will be delivered to login page