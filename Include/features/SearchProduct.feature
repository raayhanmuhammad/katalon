@SearchProduct
Feature: Search Product
	As a user, I want to search product in Secondhand Web.
	
	@SPO001
	Scenario: SPO001 - User want to see all product
		Then User click on SEMUA button
		
	@SPO002
	Scenario: SPO002 - User want to see all baju category product
		Then User click on BAJU button
		
	@SPO003
	Scenario: SPO003 - User want to see all hoby category product
		Then User click on HOBY button
		
	@SPO004
	Scenario: SPO004 - User want to see all kendaraan category product
		Then User click on KENDARAAN button
		
	@SPO005
	Scenario: SPO005 - User want to see all elektronik category product
		Then User click on ELEKTRONIK button
		
	@SPO006
	Scenario: SPO006 - User want to see all kesehatan category product
		Then User click on KESEHATAN button