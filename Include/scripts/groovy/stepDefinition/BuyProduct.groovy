package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class BuyProduct {

	@Then("Buyer click on Masuk button at Homepage")
	public void buyer_click_on_Masuk_button_at_Homepage() {
		WebUI.callTestCase(findTestCase('Pages/Page_Home/Click Masuk'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Buyer input registered email {string}")
	public void buyer_input_registered_email(String email) {
		WebUI.callTestCase(findTestCase('Pages/Page_Login/Input Email'), [('email') : email_list], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Buyer input registered password {string}")
	public void buyer_input_registered_password(String password) {
		WebUI.callTestCase(findTestCase('Pages/Page_Login/Input Password'), [('password') : password_list], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Buyer click on Masuk button at Login page")
	public void buyer_click_on_Masuk_button_at_Login_page() {
		WebUI.callTestCase(findTestCase('Pages/Page_Login/Click Masuk'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Buyer choose the product")
	public void buyer_choose_the_product() {
		WebUI.callTestCase(findTestCase('Pages/Page_Home/Choose Product'), [('item') : 'Blender'], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Buyer click Saya tertarik dan ingin nego button")
	public void buyer_click_Saya_tertarik_dan_ingin_nego_button() {
		WebUI.callTestCase(findTestCase('Pages/Page_Product_Detail/Click Saya tertarik dan ingin nego'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Buyer input Prefered Price")
	public void buyer_input_Prefered_Price() {
		WebUI.callTestCase(findTestCase('Pages/Page_Product_Detail/Input Prefered Price'), [('prefered_price') : '999900'], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Buyer click Kirim Button")
	public void buyer_click_Kirim_Button() {
		WebUI.callTestCase(findTestCase('Pages/Page_Product_Detail/Click Kirim Button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Success Alert {string} will be shown")
	public void success_Alert_will_be_shown(String string) {
		WebUI.callTestCase(findTestCase('Pages/Page_Product_Detail/Read Success Alert'), [('expected') : 'Harga tawarmu berhasil dikirim ke penjual'],
		FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Buyer will be delivered to login page")
	public void buyer_will_be_delivered_to_login_page() {
		WebUI.callTestCase(findTestCase('Pages/Page_Login/Verify Content'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}