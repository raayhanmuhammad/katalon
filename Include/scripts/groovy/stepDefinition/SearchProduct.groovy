package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class SearchProduct {

	@Then("User click on SEMUA button")
	public void user_click_on_SEMUA_button() {
		WebUI.callTestCase(findTestCase('Pages/Page_Home/Click Semua Category'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click on BAJU button")
	public void user_click_on_BAJU_button() {
		WebUI.callTestCase(findTestCase('Pages/Page_Home/Click Baju Category'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click on HOBY button")
	public void user_click_on_HOBY_button() {
		WebUI.callTestCase(findTestCase('Pages/Page_Home/Click Hoby Category'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click on KENDARAAN button")
	public void user_click_on_KENDARAAN_button() {
		WebUI.callTestCase(findTestCase('Pages/Page_Home/Click Kendaraan Category'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click on ELEKTRONIK button")
	public void user_click_on_ELEKTRONIK_button() {
		WebUI.callTestCase(findTestCase('Pages/Page_Home/Click Elektronik Category'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click on KESEHATAN button")
	public void user_click_on_KESEHATAN_button() {
		WebUI.callTestCase(findTestCase('Pages/Page_Home/Click Kesehatan Category'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}