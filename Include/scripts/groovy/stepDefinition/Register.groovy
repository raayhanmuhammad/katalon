package stepDefinition

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.Then

public class Register {


	@Then("User click on link Daftar at Login page")
	public void user_click_on_link_Daftar_at_Login_page() {

		WebUI.callTestCase(findTestCase('Pages/Page_Login/Click Link Daftar'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click on Daftar button at Register page")
	public void user_click_on_Daftar_button_at_Register_page() {

		WebUI.callTestCase(findTestCase('Pages/Page_Register/Click Daftar'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Error message will be shown {string} at Nama field")
	public void error_message_will_be_shown_at_Nama_field(String message) {

		WebUI.callTestCase(findTestCase('Pages/Page_Register/Read Required Field Nama Message'), [('exp_message') : message],
		FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input Email {string}")
	public void user_input_Email(String email) {

		WebUI.callTestCase(findTestCase('Pages/Page_Register/Input Email'), [('email') : email], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input password {string}")
	public void user_input_password(String password) {

		WebUI.callTestCase(findTestCase('Pages/Page_Register/Input Password'), [('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input Nama {string}")
	public void user_input_Nama(String nama) {

		WebUI.callTestCase(findTestCase('Pages/Page_Register/Input Nama'), [('nama') : nama], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input email {string}")
	public void user_input_email(String email) {

		WebUI.callTestCase(findTestCase('Pages/Page_Register/Input Email'), [('email') : email], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User password {string}")
	public void user_password(String password) {

		WebUI.callTestCase(findTestCase('Pages/Page_Register/Input Password'), [('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input nama {string}")
	public void user_input_nama(String nama) {

		WebUI.callTestCase(findTestCase('Pages/Page_Register/Input Nama'), [('nama') : nama], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input email with inclomplete format {string}")
	public void user_input_email_with_inclomplete_format(String email) {

		WebUI.callTestCase(findTestCase('Pages/Page_Register/Input Email'), [('email') : email], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("popup meessage will be shown {string}")
	public void popup_meessage_will_be_shown(String message) {

		WebUI.callTestCase(findTestCase('Pages/Page_Register/Read Popup Error Message'), [('exp_popupMessage') : message],
		FailureHandling.STOP_ON_FAILURE)
	}

	@Then("popup message will be shown {string}")
	public void popup_message_will_be_shown(String message) {

		WebUI.callTestCase(findTestCase('Pages/Page_Register/Read Popup Error Message'), [('exp_popupMessage') : message],
		FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click on link Masuk disini")
	public void user_click_on_link_Masuk_disini() {

		WebUI.callTestCase(findTestCase('Pages/Page_Register/Click Link Masuk'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
